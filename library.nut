class CoreUtils extends GSLibrary{
	function GetAuthor()		{ return "NCarson"; }
	function GetName()			{ return "CoreUtils"; }
	function GetDescription() 	{ return "Basic utility functions and data types to make Squirrel easier to use "; }
	function GetCategory() 		{ return "util" }
	function GetVersion()		{ return 10; }
	function GetDate()			{ return "2023-04-02"; }
	function CreateInstance()	{ return "CoreUtils"; }
	function GetShortName()		{ return "CUTL"; } // Replace this with your own unique 4 letter string
	function GetAPIVersion()	{ return "1.3"; }
	function GetURL()			{ return ""; }

	function GetSettings() {
	}
}
RegisterLibrary(CoreUtils());
